import java.text.DecimalFormat;
import java.util.Scanner;

public class Fahrkartenautomat {

	static String bvg = "#########,  ###      ###    #########\n" + "###   ####  ####    ####  ####       \n"
			+ "###    ###   ###    ###   ###        \n" + "########     ####  ###   ####  ######\n"
			+ "###   ####    ###  ###   ####     ###\n" + "###    ###     ######     ###     ###\n"
			+ "##########     ######     ###########\n\n" + "#####################################\n"
			+ "#         Fahrkartenautomat         #\n" + "#####################################\n";

	static String menu = "#####################################\n" + "#            Auswahlmenu            #\n"
			+ "#####################################\n" + "# 1 # Einzelfahrschein     [ 2,90€] #\n"
			+ "# 2 # Tageskarte           [ 8,60€] #\n" + "# 3 # Kleingruppen-TK      [23,50€] #\n"
			+ "#####################################\n";

	Fahrkartenautomat() {

		init();

	}

	static void init() {
		System.out.println(bvg);

		double zuZahlenderBetrag = fahrkartenbestellungErfassen();

		if (zuZahlenderBetrag > 0) {
			double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
		}

	}

	static double fahrkartenbestellungErfassen() {

		int anzahlFahrkarten;
		double zuZahlenderBetrag = 0.0d;
		boolean isValid = false;
		Scanner tastatur = new Scanner(System.in);

		while (isValid != true) {
			System.out.println(menu);

			System.out.print("Fahrschein wählen : ");
			int option = tastatur.nextInt();

			isValid = true;

			switch (option) {

				case 1:
					zuZahlenderBetrag = 2.90d;
					break;

				case 2:
					zuZahlenderBetrag = 8.60d;
					break;

				case 3:
					zuZahlenderBetrag = 23.50d;
					break;

				default:
					System.out.println("Fehlerhafte Eingabe");
					option = tastatur.nextInt();
					isValid = false;
					break;
			}
		}

		/*
		 * System.out.print("Zu zahlender Betrag (EURO): "); zuZahlenderBetrag =
		 * tastatur.nextDouble();
		 */
		System.out.print("Anzahl der Fahrkarten : ");
		anzahlFahrkarten = tastatur.nextInt();

		return (zuZahlenderBetrag * anzahlFahrkarten);

	}

	static double fahrkartenBezahlen(double zuZahlen) {

		double eingeworfeneMünze;
		double eingezahlterGesamtbetrag = 0.0d;
		DecimalFormat betrag = new DecimalFormat("##0.00€");
		Scanner tastatur = new Scanner(System.in);

		while (eingezahlterGesamtbetrag < zuZahlen) {
			System.out.println("Noch zu zahlen: " + betrag.format(zuZahlen - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}

		fahrkartenAusgeben();

		return eingezahlterGesamtbetrag;

	}

	static void fahrkartenAusgeben() {

		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 37; i++) {
			System.out.print("=");
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");

	}

	static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {

		double rückgabebetrag;
		DecimalFormat betrag = new DecimalFormat("##0.00€");

		rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (rückgabebetrag > 0.0) {
			System.out.println("Der Rückgabebetrag in Höhe von " + betrag.format(rückgabebetrag));
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rückgabebetrag -= 0.05;
			}
		}

		Scanner inp = new Scanner(System.in);

		System.out.println("Brauchen sie noch ein Fahrschein?");
		System.out.print("Y/N ");

		String option = inp.nextLine();

		if (option.toUpperCase().equals("Y")) {
			init();
		} else {
			beenden();
		}

	}

	static void beenden() {
		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");
	}

}
