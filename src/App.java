import java.util.Scanner;

public class App {

	static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) throws Exception {
		mainpage();
	}

	static void mainpage() {

		System.out.println("##########################");
		System.out.println("# Repository Mainmenu    #");
		System.out.println("# Deniz Malik - FI-C 02  #");
		System.out.println("##########################\n\n");

		System.out.println("Wähle eine Aufgabe aus");
		System.out.println("1 - Fahrkartenautomat");
		System.out.println("2 - Methodenübung");
		System.out.println("3 - Kopfschleifen");

		System.out.print("\nAuswahl : ");
		int option = scanner.nextInt();

		switch (option) {
			case 1 -> new Fahrkartenautomat();
			case 2 -> new Methodenuebung();
			case 3 -> new Kopfschleifen();
		}

	}

}
