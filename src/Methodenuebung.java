import java.util.Scanner;

public class Methodenuebung {

	static Scanner scanner;

	Methodenuebung() {

		scanner = new Scanner(System.in);
		mainpage();
	}

	static void mainpage() {
		System.out.println("##########################");
		System.out.println("# AB - Auswahlstrukturen #");
		System.out.println("# Deniz Malik - FI-C 02  #");
		System.out.println("##########################\n\n");

		System.out.println("Wähle eine Aufgabe aus");
		System.out.println("Aufgabe 1 - Monate");
		System.out.println("Aufgabe 2 - Steuersatz");

		System.out.print("\nAuswahl : ");
		int option = scanner.nextInt();

		switch (option) {
			case 1 -> task1();
			case 2 -> task2();
			case 10 -> task10();
		}
	}

	static void task1() {

		divide();

		String[] months = { "Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September",
				"Oktober", "November", "Dezember" };

		System.out.println("######################");
		System.out.println("# Aufgabe 1 - Monate #");
		System.out.println("######################\n\n");

		System.out.print("Wähle eine Zahl zwischen 1 und 12 : ");
		int option = scanner.nextInt();

		System.out.println("\nDu hast den " + months[option - 1] + " gewählt");

		scanner.next();

		divide();
		mainpage();

	}

	static void task2() {

		divide();

		float steuersatz_voll = 0.19f;
		float steuersatz_erma = 0.07f;

		System.out.println("##########################");
		System.out.println("# Aufgabe 2 - Steuersatz #");
		System.out.println("##########################\n\n");

		System.out.print("Bitte gebe deinen Nettowert an : ");
		float netto = scanner.nextFloat();

		System.out.println("Willst du den ermäßigten oder vollen Steuersatz verwenden? Y = ermäßigt / N = voll");
		String option = scanner.next();

		if (option.toUpperCase().equals("Y")) {

			float brutto = netto * steuersatz_erma;
			System.out.println("Ihr Bruttowert ist = " + (netto - brutto));

		} else if (option.toUpperCase().equals("N")) {

			float brutto = netto * steuersatz_voll;
			System.out.println("Ihr Bruttowert ist = " + (netto - brutto));

		} else {
			System.out.println("Irgendwas ist schief gelaufen! :(");
		}

		scanner.next();
		divide();
		mainpage();

	}

	static void task10() {

		divide();

		System.out.println("################################");
		System.out.println("# Aufgabe 10 - Römische Zahlen #");
		System.out.println("################################\n\n");

	}

	static void divide() {
		System.out.println("\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
	}

}
