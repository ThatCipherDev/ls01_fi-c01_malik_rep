import java.util.Scanner;

public class Kopfschleifen {

	static Scanner scanner;

	Kopfschleifen() {

		scanner = new Scanner(System.in);
		mainpage();
	}

	static void mainpage() {
		System.out.println("##########################");
		System.out.println("# AB - Kopfschleifen     #");
		System.out.println("# Deniz Malik - FI-C 02  #");
		System.out.println("##########################\n\n");

		System.out.println("Wähle eine Aufgabe aus");
		System.out.println("Aufgabe 8 - Seitenlänge");

		System.out.print("\nAuswahl : ");
		int option = scanner.nextInt();

		switch (option) {
			case 8 -> seitenleange();
		}
	}

	static void seitenleange() {
		System.out.println("Seitenlänge");
	}

}
